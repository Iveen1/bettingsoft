import json
import os
import time
import uuid
import aiohttp
from fastapi import HTTPException
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from src.database import get_db
from src.models import Bet, BetStatuses
from src.schemas.bet import BetModel

async def createBet(bet: BetModel, db: AsyncSession): # TODO
    async with aiohttp.ClientSession() as session:
        resp = await session.get(f'{os.getenv("LINE_PROVIDER_URL")}/event/{bet.event_id}')
    if resp.status == 200:
        if not (await resp.json()).get("deadline") > time.time():
            raise HTTPException(status_code=400, detail="You didn't make it to the deadline.")
    else:
        raise HTTPException(status_code=404, detail="Event not found.")

    bet = Bet(id=uuid.uuid4(), event_id=bet.event_id, amount=bet.amount)
    db.add(bet)
    await db.commit()
    await db.close()
    return bet

async def findAll(db: AsyncSession):
    result = await db.execute(select(Bet).order_by(Bet.id))
    await db.close()
    return result.scalars().all()

async def updateBetStatuses(message):
    db: AsyncSession = await anext(get_db())
    message = json.loads(message)
    await db.execute(update(Bet).where(Bet.event_id == message["event_id"]).values({"status": BetStatuses(message["status"])}).returning(Bet))
    await db.commit()
    await db.close()
