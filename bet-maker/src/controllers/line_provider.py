import os
import aiohttp
from fastapi import APIRouter
from fastapi_cache.decorator import cache

router = APIRouter(prefix="/events")

@router.get("/")
@cache(expire=60)
async def getAllBets():
    async with aiohttp.ClientSession() as session:
        async with session.get(f'{os.getenv("LINE_PROVIDER_URL")}/events') as resp:
            return await resp.json()
