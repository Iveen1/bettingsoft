import asyncio

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Session
from src.database import get_db
from src.schemas.bet import BetModel
from src.services.bet_service import createBet, findAll

router = APIRouter()

@router.post("/bet")
async def makeBet(bet: BetModel, db: Session = Depends(get_db)):
    return (await createBet(bet=bet, db=db)).json

@router.get("/bets")
async def getAllBets(db: AsyncSession = Depends(get_db)):
    return await findAll(db)
