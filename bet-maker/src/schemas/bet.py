from decimal import Decimal
from typing import Annotated
from pydantic import BaseModel, Field


class BetModel(BaseModel):
    event_id: int
    amount: Annotated[Decimal, Field(decimal_places=2, gt=0)]
