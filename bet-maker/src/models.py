import enum
import uuid
from sqlalchemy import Column, Double, Integer, Enum, DateTime, func
from sqlalchemy.dialects.postgresql import UUID
from src.database import Base

class BetStatuses(enum.Enum):
    NOT_PLAYED = 1
    WIN = 2
    LOSS = 3


class Bet(Base):
    __tablename__ = "bets"

    id = Column(UUID(as_uuid=True), default=uuid.uuid4, primary_key=True)
    event_id = Column(Integer, nullable=False)
    amount = Column(Double, nullable=False)
    status = Column(Enum(BetStatuses), nullable=False, default=BetStatuses.NOT_PLAYED)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

    @property
    def json(self):
        return {
            "id": self.id,
            "event_id": self.event_id,
            "amount": self.amount,
            "status": self.status,
            "created_at": self.created_at,
            "updated_at": self.updated_at
        }