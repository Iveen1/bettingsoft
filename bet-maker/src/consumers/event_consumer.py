import asyncio
from ..consumers.rabbitmq_client import RabbitMQConnection
from ..services.bet_service import updateBetStatuses


async def on_message(message):
    await updateBetStatuses(message.body)


async def main():
    client = RabbitMQConnection()
    await client.connect()
    channel = await client.get_channel()

    declare_ok = await channel.queue_declare('events_updates')
    await channel.basic_consume(
        declare_ok.queue, on_message, no_ack=True
    )


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.run_forever()
