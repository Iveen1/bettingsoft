from redis import asyncio as aioredis
from fastapi import FastAPI, APIRouter
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from src.config import Settings
from src.controllers.bet import router as bet_router
from src.controllers.line_provider import router as line_provider_router

settings = Settings()
app = FastAPI()

@app.on_event("startup")
async def startup():
    redis = aioredis.from_url(f"redis://{settings.REDIS_HOST}:{settings.REDIS_PORT}", encoding="utf8", decode_responses=True)
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")

router = APIRouter(prefix="/api")
router.include_router(bet_router)
router.include_router(line_provider_router)

app.include_router(router)
