from typing import Any
from dotenv import load_dotenv
from pydantic_settings import BaseSettings

class Settings(BaseSettings):
    def __init__(self, **values: Any):
        load_dotenv()
        super().__init__(**values)


    DB_HOST: str
    DB_PORT: int
    DB_NAME: str
    DB_USERNAME: str
    DB_PASSWORD: str
    REDIS_HOST: str
    REDIS_PORT: int
    LINE_PROVIDER_URL: str

    class Config:
        env_file = ".env"


