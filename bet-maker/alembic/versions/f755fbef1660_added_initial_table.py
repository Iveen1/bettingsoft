"""Added initial table

Revision ID: f755fbef1660
Revises: 
Create Date: 2024-03-03 23:22:42.760524

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'f755fbef1660'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('bets',
    sa.Column('id', sa.UUID(), nullable=False),
    sa.Column('event_id', sa.Integer(), nullable=False),
    sa.Column('amount', sa.Double(), nullable=False),
    sa.Column('status', sa.Enum('NOT_PLAYED', 'WIN', 'LOSS', name='betstatuses'), nullable=False),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=True),
    sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('bets')
    # ### end Alembic commands ###
