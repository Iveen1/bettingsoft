import json
from producers.rabbitmq_client import RabbitMQConnection

async def eventProducer(event_id, status):
    client = RabbitMQConnection()
    await client.connect()
    channel = await client.get_channel()
    await channel.queue_declare("events_updates")
    await channel.basic_publish(body=json.dumps({'event_id': event_id, 'status': status}).encode('utf-8'), routing_key="events_updates")
