import asyncio
import aiormq


class RabbitMQConnection:
    _instance = None

    def __new__(cls, host="rabbitmq", port=5672, username="guest", password="guest"):
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, host="rabbitmq", port=5672, username="guest", password="guest"):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.connection = None

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    async def connect(self):
        retries = 0
        while retries < 10:
            try:
                self.connection = await aiormq.connect(f"amqp://{self.username}:{self.password}@{self.host}:{self.port}/")
                return
            except Exception:
                retries += 1
                await asyncio.sleep(10 * retries)

    async def is_connected(self):
        return self.connection is not None and self.connection.is_opened

    async def close(self):
        if self.is_connected():
            await self.connection.close()
            self.connection = None

    async def get_channel(self):
        if await self.is_connected():
            return await self.connection.channel()
        return None
